<?php
//============================ OAuth2 settings ============================\\
// Your client ID and Secret: 
// https://auth.vatsim.net/ -> Organisations/Own -> View -> OAUTH
$appClientID				= '111';
$appClientSecret			= 'abcdefghijklmnopqrstuvwxyz1234567890';
$appRedirectUrl				= 'https://yourwebsite.url';


//============================ Plugin settings ============================\\
// Redirect when user logs out.
$logoutURL					= 'https://yourwebsite.url'; 

// The user did not provide enough data (in the VATSIM Connect Authorization screen).
$loginDeniedURL				= 'https://yourwebsite.url/login-denied/';


//============================ Advanced settings ===========================\\

// WordPress metatag for User's last visit and IP logging
$userLastVisit				= '';//'last_visit'; //emty = do not log
$userIPAddress				= '';//'ip_client';  //emty = do not log



//================ DO NOT CHANGE UNLESS ABSOLUTELY NECESARRY ===============\\
// no need to change this, more info here:
// https://github.com/vatsimnetwork/documentation/blob/master/connect.md
$appScope					= 'full_name email vatsim_details'; // country'; <- atm not used by VATSIM?

$EndpointAuthorize			= 'https://auth.vatsim.net/oauth/authorize';
$EndpointToken				= 'https://auth.vatsim.net/oauth/token';
$EndpointUser				= 'https://auth.vatsim.net/api/user';

?>
