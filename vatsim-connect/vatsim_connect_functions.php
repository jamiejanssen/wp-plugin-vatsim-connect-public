<?php
function vc_user(	$existing_user, $user, $vat_cid, $vat_email, 
					$vat_name_first, $vat_name_last, $userIPAddress){
	
	//======================== WordPress EXISTING USER ==========================\\
	if ($existing_user== TRUE) {														// Check existing user
		$user_id = $user->ID;
		wp_update_user(	array('ID' => $user_id, 'user_email' => $vat_email ) ); 		// ALWAYS Update user's e-mail address

	//======================== WordPress NEW USER ===============================\\
	} else { 																			// New user, if ($vat_user=='New')
		
		if(get_user_by('email', $vat_email)){											// Checks for existing user with same e-mail address
			exit('ERROR! User with same e-mail address already exists!');
		}
		
		$random_password = wp_generate_password( 32, true );							// Generate random password for wp_users table
		$user_id		 = wp_create_user($vat_cid, $random_password, $vat_email);
		
		$u = new WP_User($user_id);
		$u->add_role('visitor'); 														// User default role is Visitor
		$u->add_role('vatsim_member');													// CUSTOM role! Use extra plugin for this e.g. Member Roles (https://wordpress.org/plugins/members/)
		
		$info		= get_userdata($user_id);
		$disp_name	= trim($info->first_name .' '. $info->last_name);					// First name and Last name as Display name in WP

		add_user_meta( $user_id, 'reg_date', current_time( 'mysql', true ), true);		// Write VATSIM reg_date (first DV-site logon time) to usermeta.	 												// User default role is Vatsim_member
	}
	
	//======================== WordPress ALL USERS ==============================\\
	update_user_meta( $user_id, 'first_name', 	$vat_name_first) ; 						// Always update First name
	update_user_meta( $user_id, 'last_name',	$vat_name_last ) ;						// Always update Last name
	if ($userIPAddress){
		$ip_client	= $_SERVER['REMOTE_ADDR'];											// Log the user his IP
		update_user_meta( $user_id, $userIPAddress, $ip_client ) ;						// Log IP address user if set in config.php
	}
}



function vc_validate($vat_cid){
	$user 		= get_user_by('login', $vat_cid); 										// Get user object from wp_users table by his CID
	$user_id 	= $user->ID;

	//======================== Logon User function ==============================\\
	function vc_user_logon($user_id, $user){
		wp_set_current_user($user_id);
		wp_set_auth_cookie( $user_id, true );
		do_action( 'wp_login', $user->user_login, $user );	
	}

	//======= Force WordPress Display name publicly as Firstname Lastname =======\\
	$info		= get_userdata($user_id);
	$disp_name	= trim($info->first_name .' '. $info->last_name);						// First name and Last name as Display Name in wp_usermeta
	$args 		= array('ID' => $user_id,'display_name' => $disp_name);
	wp_update_user( $args );															// This updates the WP_Users table's 'display_name'
		
	vc_user_logon($user_id, $user);														// comment this if you use special Subsdivision permissions on your site.


}
?>