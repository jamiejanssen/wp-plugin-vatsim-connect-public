<?php
/**
* Plugin Name: -= VATSIM Connect Public =-
* Plugin URI: http://www.vatsim.net
* Description: VATSIM Connect OAuth2 plugin for WordPress 3.7+ brought to you by the Dutch VACC.
* Version: 1.2.09
* Author: Jamie Janssen - Dutch VACC - 2021©
* Author URI: https://www.jamiejanssen.nl
* License: None
* License URI: None
*/
require('vatsim_connect_handler.php'); 													// the actual OAuth2 handler

function vc_oauth2_login(){
	require('config.php');
	require('vatsim_connect_functions.php');
	
	if ((is_user_logged_in())&&$userLastVisit){
		update_user_meta(get_current_user_id(), $userLastVisit, date("Y-m-d H:i:s"));	// last visit date/time
	}
	
	if(isset($_REQUEST['vatsim'])) { 													// listen to https://website.url/?vatsim= for login/logout
		$code	=$_GET['vatsim'];
		if ($code=='logout') {															// respond to this link /?vatsim=logout and 
			wp_logout();																// logout the current user
			header('Location: '.$logoutURL);											// then redirect to the logout page.
			exit();
		}
		if ($code=='login') {															// respond to this link /?code=login 
			
			header('Location: '.$EndpointAuthorize.'?client_id='.$appClientID.'&scope='
					.$appScope.'&redirect_uri='.$appRedirectUrl.'&response_type=code');	// redirect to the oauth2 server
			
			exit();
		}
	}

	if(isset($_REQUEST['code'])) {														// listen to https://redirect.url/?code= for the OAuth2 stuff
		$code	=$_GET['code']; 		
		if(session_id() == '' || !isset($_SESSION))
			session_start();
		try {
			//Generate access token
			$oauth_handler = new vc_handler();											// the vatsim_connect_handler.php
			
			$accessToken   = $oauth_handler->getAccessToken($EndpointToken, 
					'authorization_code', $appClientID, $appClientSecret, $code, 
					$appRedirectUrl);
		
			//Error handling
			if(!$accessToken)
				exit('Invalid token.');
			if (substr($EndpointUser, -1) == "=") {
				$EndpointUser .= $accessToken;
			}
			//====================== Populate variables =============================\\
			//VATSIM user data array based on given scope
			$ContentArray 	= $oauth_handler->getContent($EndpointUser, $accessToken);
			
			//The contents of the array
			$vat_email		= $ContentArray['data']['personal']['email'];
			$vat_cid		= $ContentArray['data']['cid'];
			$vat_name_first = $ContentArray['data']['personal']['name_first'];
			$vat_name_last	= $ContentArray['data']['personal']['name_last'];
			$vat_data		= $ContentArray['data']['vatsim'];							// VATSIM Details
			$vat_subdiv		= $ContentArray['data']['vatsim']['subdivision']['id']; 	// SubDivision: use this var to check
																						// if the user is member of your VACC.
			$vat_oauth_token= filter_var($ContentArray['data']['oauth']['token_valid'], 
									FILTER_VALIDATE_BOOLEAN); 							// 'true or false' -> TRUE or FALSE
			
			//Other vars
			$existing_user	= FALSE; 													// Set existing WP user initially to FALSE
			$vat_dvm 		= FALSE; 													// Set existing DV_Member initially to FALSE
			$user			= get_user_by( 'login', $vat_cid ); 						// Empty if WordPress user is new!
			
		 //=============== DEBUG: Uncomment to view the Array  ======================\\
		 //	echo'<pre>';print_r($ContentArray);echo'</pre>';
		 //	exit();
		 //=============== DEBUG: Uncomment to view the Array  ======================\\



			//=============== VATSIM Authorization Request Checkmarks ===============\\
			//Members need to fully authorize, otherwise redirect to DeniedURL

			if (!$vat_oauth_token||
				empty($vat_name_first)||
				empty($vat_name_last)||
				empty($vat_email)){														// We need at least these items otherwise reject user
						
				wp_redirect($loginDeniedURL);
				exit();
			}
			//============== Existing WordPress user TRUE of FALSE? =================\\
			if ( $user ) {	//Check existing User and/or Division Member or new WP user.								

			$existing_user = TRUE;														// Existing user, thus take special care
			} 
			
			vc_user($existing_user	, $user, $vat_cid,	$vat_email, $vat_name_first, 
					$vat_name_last, $userIPAddress); 									// Add or Update the user into the WP_Users/ WP_Usermeta/ other WP_dbs
			
			//============ Validate and login the user to the WP site  ==============\\
			vc_validate($vat_cid); 														// Login User
			
			wp_redirect(home_url());													// Redirect User to homepage after successfull Login
			exit();
		
		} catch (Exception $e) {														// Login failed for whatever reason.
			exit($e->getMessage());
		}
	}
}
add_action( 'init', 'vc_oauth2_login' );