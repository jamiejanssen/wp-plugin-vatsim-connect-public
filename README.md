# VATSIM Connect - A WordPress OAuth2 plugin

## Content
1. Requirements
2. File description
3. Setup
4. Usefull info

### 1 - Requirements
a. Wordpress 5.x+
b. Authorization server. In this case VATSIM.
c. A decent text editor :)

### 2 - File description

#### Config.php
Edit this file only.

#### vatsim_connect.php
You might want to alter this file if you know what you are doing.

#### vatsim_connect_functions.php
Same for this file.

#### vatsim_connect_handler.php
No need to edit this one, contains the OAuth2 stuff.

### 3 - HowTo
Edit the config.php and update the settings with your website info.

Install the plugin, upload to /wp-content/plugins/vatsim-connect/<place the 4 files here>

Login with VATSIM Connect, just add /?vatsim=login to the end of your homepage URL e.g.:
https://yourwebsite.url/?vatsim=login

Logout:
https://yourwebsite.url/?vatsim=logout

### 4 - (Not so) Usefull info
Authorization link:
https://auth.vatsim.net/oauth/authorize?client_id=121&scope=full_name%20email%20vatsim_details%20country&redirect_uri=https://yourwebsite.url&response_type=code

The wp-username will be the vatsim CID. Displayed (Forum etc) will be your full name. Email will be stored as well.
WP needs a password for its database, regardless of the OATH2 authentication, this is a randomized 32 characters long password.
I suggest you create a standard WP backend administrator, you can assign any (Vatsim) user as Administrator later on.
Secure your /wp-login.php or /wp-admin URLs, there are plenty of ways of doing this. Google it.

You could use and store any data Vatsim Connect offers, see below for current return Array:

Return Array:
Array ( 

[data] => Array ( [cid] => 1234567 

[personal] => Array ( [name_first] => Jamie [name_last] => Janssen [name_full] => Jamie Janssen [email] => jamiejanssen@emailaddress.com

[country] => Array ( [id] => NL [name] => Netherlands ) ) 

[vatsim] => Array ( [rating] => Array ( [id] => 2 [long] => Tower Trainee [short] => S1 ) 

[pilotrating] => Array ( [id] => 0 ) 

[division] => Array ( [id] => EUD [name] => Europe (except UK) )

[region] => Array ( [id] => EUR [name] => Europe ) 

[subdivision] => Array ( [id] => NETH [name] => Dutch ) ) 

[oauth] => Array ( [token_valid] => true ) ) )
