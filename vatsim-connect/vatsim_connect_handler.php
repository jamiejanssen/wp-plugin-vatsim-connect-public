<?php
class vc_handler {
	function getAccessToken($tokenendpoint, $grant_type, $clientid, $clientsecret, $code, $redirect_url){
		
		$response = wp_remote_post( $tokenendpoint, array(
			'method'		=> 'POST',
			'timeout'		=> 60,
			'redirection'	=> 5,
			'httpversion'	=> '1.1',
			'blocking'		=> true,
			'headers'		=> array(),
			'body'			=> array(
				'grant_type'	=> 'authorization_code',
				'code'			=> $code,
				'client_id'		=> $clientid,
				'client_secret'	=> $clientsecret,
				'redirect_uri'	=> $redirect_url
			),
			'cookies'     => array()
		) );
		
		//Error handling
		if(!is_array($response)){	
			exit('Invalid response received.');
		}
		$response =  $response['body'] ;

		if(!is_array(json_decode($response, true))){
			echo '<b>Response: </b><br>';print_r($response);echo '<br><br>';
			exit('Invalid response received.');
		}
		
		$content = json_decode($response,true);

		if(isset($content['error_description'])){
			exit($content['error_description']);
		} else if(isset($content['error'])){
			exit($content['error']);
		} else if(isset($content['access_token'])) {
			$access_token = $content['access_token'];
		} else {
			echo '<b>Response: </b><br>';print_r($content);echo '<br><br>';
			exit('Invalid response received from VATSIM Connect.');
		}
	return $access_token;
	}
	
	function getContent($ContentArray, $access_token){
		$response   = wp_remote_post( $ContentArray, array(
			'method'      => 'GET',
			'timeout'     => 45,
			'redirection' => 5,
			'httpversion' => '1.1',
			'blocking'    => true,
			'headers'     => array(
			'Authorization' => 'Bearer '.$access_token
			),
			'cookies'     => array()
		) );

		$response =  $response['body'];

		if(!is_array(json_decode($response, true))){
		    $response = addcslashes($response, '\\');
            if(!is_array(json_decode($response, true))){
                echo '<b>Response : </b><br>';print_r($response);echo '<br><br>';
                exit("Invalid response received.");
            }
		}

		$content = json_decode($response,true);
		if(isset($content['error_description'])){
			exit($content['error_description']);
		} else if(isset($content['error'])){
			exit($content['error']);
		}
	return $content;
	}
}
?>